import pymel.core as pm

nodes = [
    "OpenVDBRead",
    "OpenVDBTransform",
    "OpenVDBCopy",
    "OpenVDBFilter",
    "OpenVDBFromMayaFluid",
    "OpenVDBFromPolygons",
    "OpenVDBWrite"
    ]

prev_select = pm.ls(l=True, sl=True, type=nodes, tail=1)

pm.createNode("OpenVDBToPolygons", n="OpenVDBToPolygons1")
new = pm.ls(l=True, sl=True, type=u"OpenVDBToPolygons")[0]

if prev_select != []:
    pm.connectAttr(prev_select[0] + u".VdbOutput", new + u".vdbInput")
